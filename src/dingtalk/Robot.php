<?php
/**
 * 钉钉机器人
 * @authors 孙振环 (szhcool1129@sina.com)
 * @date    2021-05-31 15:57:05
 * @version $Id$
 */
namespace Kunlun\dingtalk;

use Exception;

class Robot {

    const string SEND_URL = 'https://oapi.dingtalk.com/robot/send';

    /**
     * 接入token
     */
    private string $accessToken;

    /**
     * 加签密钥
     */
    private string $secret;

    private string $timestamp;

    public function __construct(string $accessToken, string $secret) {
        $this->accessToken = $accessToken;
        $this->secret = $secret;
        $this->timestamp = time() . '000';
    }

    /**
     * 推送钉钉消息
     * @param string $jsonData
     * @return bool|mixed|string [type]               [description]
     * @author 孙振环 (szhcool1129@sina.com)
     * @date   2021-05-31
     */
    public function send(string $jsonData): mixed
    {
        try {
            $sendUrl = self::SEND_URL . '?access_token=' . $this->accessToken . '&timestamp=' . $this->timestamp . '&sign=' . $this->getSignString();
            $header = ['Content-Type: application/json;charset=utf-8'];
            $response = $this->post($sendUrl, $jsonData, $header);
            $response = json_decode($response, true);
            if ($response['errcode'] == 0) {
                return true;
            } else {
                return $response['errmsg'];
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * 签名字符串
     * @return string [type]     [description]
     * @author 孙振环 (szhcool1129@sina.com)
     * @date   2021-05-31
     */
    private function getSignString(): string{
        $signString = $this->timestamp . "\n" . $this->secret;
        $sign = hash_hmac('sha256', $signString, $this->secret, true);
        return urlencode(base64_encode($sign));
    }

    /**
     * 发送post请求
     * @author 孙振环 (szhcool1129@sina.com)
     * @date   2020-07-09
     * @param  string      $url     请求地址
     * @param  array       $data    请求参数
     * @param  array       $header  请求头
     * @param  int|integer $timeout 超时时间
     * @return mix
     */
    private function post(string $url, $data, array $header = [], int $timeout = 30): string{
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $header && curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.1 Safari/537.11');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }
}
