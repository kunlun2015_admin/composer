<?php
/**
 * 日志组件
 * @author 孙振环 (szhcool1129@sina.com)
 * @date    2023-03-21 09:54:50
 * @version $Id$
 */

namespace Kunlun\base;

use stdClass;

class Log
{

    static private $_instance;

    private $_rootPath;

    private function __construct($rootDir)
    {
        $this->_rootPath = $rootDir ?: dirname(__FILE__, 5) . DIRECTORY_SEPARATOR . 'logs';
    }

    public static function getInstance($rootDir = ''): Log
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self($rootDir);
        }
        return self::$_instance;
    }

    /**
     *  错误日志
     * @return false|int
     */
    public function error(): false|int
    {
        $params = func_get_args();
        $template = $params[0] ?? '';
        $placeholder = array_map(function ($val) {
            if (is_array($val) || $val instanceof StdClass) {
                return json_encode($val);
            }
            return $val;
        }, array_slice($params, 1));
        $data = vsprintf(str_replace('{}', '%s', $template), $placeholder);
        return $this->write($data, 'error');
    }

    /**
     * 业务日志
     * @return false|int
     */
    public function info(): false|int
    {
        $params = func_get_args();
        $template = $params[0] ?? '';
        $placeholder = array_map(function ($val) {
            if (is_array($val) || $val instanceof StdClass) {
                return json_encode($val);
            }
            return $val;
        }, array_slice($params, 1));
        $data = vsprintf(str_replace('{}', '%s', $template), $placeholder);
        return $this->write($data, 'info');
    }

    /**
     * 写入日志
     * @param string $data 要写入的数据
     * @param string $dir 要写入的目录
     * @return false|int
     */
    private function write(string $data, string $dir = 'default'): false|int
    {
        //日志信息
        $logInfo = $this->_logWriteInfo($data, $dir);
        //同步写入
        return $this->_syncWrite($logInfo);
    }

    /**
     * 同步写入
     * @param array $logInfo
     * @return false|int
     */
    private function _syncWrite(array $logInfo): false|int
    {
        return file_put_contents($logInfo['path'], $logInfo['content'], FILE_APPEND);
    }

    /**
     * 日志写入信息
     * @param string $data 要写入的数据
     * @param string $dir 要写入的目录
     * @return array
     */
    private function _logWriteInfo(string $data, string $dir): array
    {
        //获取调用者的信息
        $callInfo = debug_backtrace();
        $callFilePath = $callFileLine = '';
        if (isset($callInfo[2])) {
            $callFilePath = $callInfo[2]['file'];
            $callFileLine = $callInfo[2]['line'];
        }

        $saveDir = $this->_writePath($dir);
        $logName = $this->_logFileName();
        $logString = date('Y-m-d H:i:s') . '--' . $data . '--' . $callFilePath . ':' . $callFileLine . PHP_EOL;
        $logFile = $saveDir . DIRECTORY_SEPARATOR . $logName;
        return ['path' => $logFile, 'content' => $logString];
    }

    /**
     * 创建写入路径并返回
     * @param string $dir 要创建的目录
     * @return string
     */
    private function _writePath(string $dir): string
    {
        $dir = $dir . DIRECTORY_SEPARATOR . date('Ym');
        $saveDir = $this->_rootPath . DIRECTORY_SEPARATOR . $dir;
        if (!is_dir($saveDir)) {
            mkdir($saveDir, 0777, true);
        }
        return $saveDir;
    }

    /**
     * 日志记录文件名
     * @return string
     */
    private function _logFileName(): string
    {
        return date('d') . '.log';
    }

    /**
     * 禁止克隆对象
     * @return
     * @author 孙振环 (szhcool1129@sina.com)
     * @date   2020-07-09
     */
    private function __clone()
    {
    }
}
