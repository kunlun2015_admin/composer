<?php
/**
 * id生成器
 * @authors Kunlun (szhcool1129@sina.com)
 * @date    2024-01-23 14:25
 */

namespace Kunlun\base;

use Exception;
use Redis;
use RedisException;

class IdGenerate
{
    private static $_instance;

    private ?Redis $redisInstance = null;

    const int TIME_START_POINT = 1704439581000; // 时间起始标记点(毫秒)，作为基准，一般取系统的最近时间（一旦确定不能变动）

    const int WORKER_ID_BITS = 5; // 机器标识位数
    const int DATACENTER_ID_BITS = 5; // 数据中心标识位数
    const int SEQUENCE_BITS = 12; // 毫秒内自增位

    private mixed $workerId; // 工作机器ID(0~31)
    private mixed $datacenterId; // 数据中心ID(0~31)
    private mixed $sequence; // 毫秒内序列(0~4095)

    private int $maxWorkerId = -1 ^ (-1 << self::WORKER_ID_BITS); // 机器ID最大值31
    private int $maxDatacenterId = -1 ^ (-1 << self::DATACENTER_ID_BITS); // 数据中心ID最大值31

    private int $workerIdShift = self::SEQUENCE_BITS; // 机器ID偏左移12位
    private int $datacenterIdShift = self::SEQUENCE_BITS + self::WORKER_ID_BITS; // 数据中心ID左移17位
    private int $timestampLeftShift = self::SEQUENCE_BITS + self::WORKER_ID_BITS + self::DATACENTER_ID_BITS; // 时间毫秒左移22位
    private int $sequenceMask = -1 ^ (-1 << self::SEQUENCE_BITS); // 生成序列的掩码4095

    private int $lastTimestamp = -1; // 上次生产id时间戳

    /**
     * @throws Exception
     */
    private function __construct($workerId = 0, $datacenterId = 0, $sequence = 0)
    {
        if ($workerId > $this->maxWorkerId || $workerId < 0) {
            throw new Exception("worker Id can't be greater than {$this->maxWorkerId} or less than 0");
        }

        if ($datacenterId > $this->maxDatacenterId || $datacenterId < 0) {
            throw new Exception("datacenter Id can't be greater than {$this->maxDatacenterId} or less than 0");
        }

        $this->workerId = $workerId;
        $this->datacenterId = $datacenterId;
        $this->sequence = $sequence;
    }

    /**
     * 初始化实例
     * @param Redis $redisInstance redis实例
     * @return IdGenerate
     */
    public static function init(Redis $redisInstance): IdGenerate
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self();
            self::$_instance->redisInstance = $redisInstance;
        }
        return self::$_instance;
    }

    /**
     * 获取全局id
     * @return string
     * @throws Exception
     */
    public function getId(): string
    {

        return $this->nextId();
    }

    /**
     * @throws Exception
     */
    private function getLastTimestamp(): void
    {
        if (!$this->redisInstance) {
            throw new Exception('未初始化redis实例');
        }
        $lastTimestamp = $this->redisInstance->get('idGenerate:lastTimestamp');
        $this->lastTimestamp = $lastTimestamp ?: $this->timeGen();
    }

    /**
     * @throws RedisException
     */
    private function setLastTimestamp($timestamp): void
    {
        $this->redisInstance->set('idGenerate:lastTimestamp', $timestamp, 1800);
        $this->lastTimestamp = $timestamp;
    }

    /**
     * @throws Exception
     */
    private function nextId(): string
    {
        $this->getLastTimestamp();
        $timestamp = $this->timeGen();
        if ($timestamp < $this->lastTimestamp) {
            $diffTimestamp = bcsub($this->lastTimestamp, $timestamp);
            throw new Exception("Clock moved backwards.  Refusing to generate id for {$diffTimestamp} milliseconds");
        }

        if ($this->lastTimestamp == $timestamp) {
            $this->sequence = ($this->sequence + 1) & $this->sequenceMask;

            if (0 == $this->sequence) {
                $timestamp = $this->tilNextMillis($this->lastTimestamp);
            }
        } else {
            $this->sequence = 0;
        }
        $this->setLastTimestamp($timestamp);
        $gmpTimestamp = \gmp_init($this->leftShift(bcsub($timestamp, self::TIME_START_POINT), $this->timestampLeftShift));
        $gmpDatacenterId = \gmp_init($this->leftShift($this->datacenterId, $this->datacenterIdShift));
        $gmpWorkerId = \gmp_init($this->leftShift($this->workerId, $this->workerIdShift));
        $gmpSequence = \gmp_init($this->sequence);
        return gmp_strval(gmp_or(gmp_or(gmp_or($gmpTimestamp, $gmpDatacenterId), $gmpWorkerId), $gmpSequence));
    }

    protected function tilNextMillis($lastTimestamp): float
    {
        $timestamp = $this->timeGen();
        while ($timestamp <= $lastTimestamp) {
            $timestamp = $this->timeGen();
        }
        return $timestamp;
    }

    protected function timeGen(): float
    {
        return floor(microtime(true) * 1000);
    }

    // 左移 <<
    protected function leftShift($a, $b): string
    {
        return bcmul($a, bcpow(2, $b));
    }
}