<?php
/**
 * http请求封装
 * @author 孙振环 (szhcool1129@sina.com)
 * @date    2023-05-06 12:56:48
 * @version $Id$
 */

namespace Kunlun\base;

use CurlHandle;
use Exception;

class Curl
{
    /**
     * 代理配置
     * 代理地址及端口 ['host' => '', 'port' => '']
     * @var array
     */
    public static array $proxy = [];

    /**
     * 是否使用代理
     * @var bool
     */
    public static bool $useProxy = false;

    /**
     * 发送get请求
     * @param string $url 请求url
     * @param array $data 请求参数
     * @param array $header header头
     * @param int $timeout 超时时间
     * @param bool $isHeader 是否开启header返回
     * @param array $proxy 代理ip及端口 ['host' => '', 'port' => '']
     * @param int $retryTimes
     * @return object
     */
    public static function get(string $url, array $data = [], array $header = [], int $timeout = 30, bool $isHeader = false, array $proxy = [], int $retryTimes = 0): object
    {
        if (!empty($data)) {
            $params = http_build_query($data);
            $url = strpos($url, '?') ? $url . '&' . $params : $url . '?' . $params;
        }
        $ch = self::beforeRequest($url, $header, $timeout, $isHeader, $proxy);
        return self::afterRequest($ch, $retryTimes);
    }

    /**
     * 发送post请求
     * @param string $url 请求地址
     * @param array|string $data 请求参数
     * @param array $header 请求头
     * @param int $timeout 超时时间
     * @param array $proxy 代理ip及端口 ['host' => '', 'port' => '']
     * @param int $retryTimes
     * @return object
     */
    public static function post(string $url, array|string $data = [], array $header = [], int $timeout = 30, array $proxy = [], int $retryTimes = 0): object
    {
        $ch = self::beforeRequest($url, $header, $timeout, false, $proxy);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, is_array($data) ? http_build_query($data) : $data);
        return self::afterRequest($ch, $retryTimes);
    }

    /**
     * 文件分片下载
     * @param string $url 下载地址
     * @param string $savePath 保存路径
     * @param int $chunkSize 分片大小(单位M)
     * @param array $proxy 代理ip及端口 ['host' => '', 'port' => '']
     * @return bool
     * @throws Exception
     */
    public static function download(string $url, string $savePath, int $chunkSize = 10, array $proxy = []): bool
    {
        //获取文件大小
        $fileSize = self::getFileSize($url, $proxy);
        echo '文件大小：' . $fileSize . PHP_EOL;
        //每次下载的大小
        $perChunkSize = 1024 * 1024 * $chunkSize;
        //分片数量
        $chunkNum = ceil($fileSize / $perChunkSize);
        echo '分片数量：' . $chunkNum . PHP_EOL;
        //开始下载
        $fp = fopen($savePath, 'wb+');
        for ($i = 0; $i < $chunkNum; $i++) {
            $chunkStart = $i * $perChunkSize;
            $chunkEnd = ($i + 1) * $perChunkSize - 1;
            $range = $chunkStart . '-' . $chunkEnd;
            echo '当前分片：' . ($i + 1) . ',range:' . $range . PHP_EOL;
            //获取分片内容
            $ch = self::beforeRequest($url, [], 300, true, $proxy);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Range: bytes=' . $range]);
            $res = curl_exec($ch);
            $resCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            if ($resCode == 200 || $resCode == 206) {
                echo '分片应返回大小：' . ($chunkEnd - $chunkStart) . '，实际返回大小：' . strlen($res) . PHP_EOL;
                fwrite($fp, $res);
            } else {
                throw new Exception('分片获取失败，分片：' . $chunkNum);
            }
        }
        fclose($fp);
        return true;
    }

    /**
     * 获取文件大小
     * @param string $url
     * @param array $proxy 代理ip及端口 ['host' => '', 'port' => '']
     * @return int
     * @throws Exception
     */
    public static function getFileSize(string $url, array $proxy = []): int
    {
        //获取文件大小
        $ch = self::beforeRequest($url, [], 30, true, $proxy);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, ["Range: bytes=[0-1]"]);
        $res = curl_exec($ch);
        $resCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($resCode == 200) {
            preg_match('/Content-Length: (\d+)/', $res, $match);
            if (isset($match[1])) {
                return (int)$match[1];
            } else {
                throw new Exception('获取文件大小失败', 1001);
            }
        } else {
            throw new Exception('请求出错', 1002);
        }
    }

    /**
     * 请求基本设置
     * @param string $url
     * @param array $header
     * @param int $timeout
     * @param bool $isHeader
     * @param array $proxy 代理ip及端口 ['host' => '', 'port' => '']
     * @return CurlHandle|false
     */
    private static function beforeRequest(string $url, array $header, int $timeout, bool $isHeader = false, array $proxy = []): CurlHandle|false
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_HEADER, $isHeader);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        //使用代理
        $proxy = self::$proxy && !$proxy ? self::$proxy : $proxy;
        if (!self::$useProxy) {
            $proxy = self::$useProxy;
        }
        if ($proxy && isset($proxy['host']) && isset($proxy['port'])) {
            curl_setopt($ch, CURLOPT_PROXY, $proxy['host']);
            curl_setopt($ch, CURLOPT_PROXYPORT, $proxy['port']);
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, self::getHeader($url, $header));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        return $ch;
    }

    /**
     * 设置请求头
     * @param string $url
     * @param array $header
     * @return array
     */
    private static function getHeader(string $url, array $header): array
    {
        $parseUrl = parse_url($url);
        $data = $header + [
                'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
                'Cache-Control' => 'no-cache',
                'Pragma' => 'no-cache',
                'Referer' => $url,
                'Host' => $parseUrl['host'] ?? '',
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/131.0.0.0 Safari/537.36'
            ];
        $result = [];
        foreach ($data as $k => $v) {
            $result[] = $k . ':' . $v;
        }
        return $result;
    }

    /**
     * 返回数据处理
     * @param $ch
     * @param int $retryTimes
     * @param int $curTimes
     * @return object
     */
    private static function afterRequest($ch, int $retryTimes, int $curTimes = 0): object
    {
        $result = new \StdClass();
        $result->response = curl_exec($ch);
        $result->error = curl_error($ch);
        $result->code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (($result->code != 200 || $result->error) && $curTimes < $retryTimes) {
            sleep(1);
            return self::afterRequest($ch, $retryTimes, $curTimes + 1);
        } else {
            curl_close($ch);
            return $result;
        }
    }
}