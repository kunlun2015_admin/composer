<?php
/**
 * excel 工具
 * @author Kunlun (szhcool1129@sina.com)
 * @date    2024-06-19 09:47
 */

namespace Kunlun\base;

use Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ExcelUtil
{
    /**
     * 读取excel内容
     * @param string $filePath 文件路径
     * @return array
     * @throws Exception
     */
    public static function read(string $filePath): array
    {
        try {
            $spreadsheet = IOFactory::load($filePath);
            return $spreadsheet->getActiveSheet()->toArray();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}