<?php
/**
 * 结果输出
 * @authors Kunlun (szhcool1129@sina.com)
 * @date    2024-01-29 16:04
 */

namespace Kunlun\base;

class Response
{
    public function __construct(private int $code, private string $msg, private array $data = [])
    {
        header('Content-Type: application/json');
        $this->data = ArrayUtil::arrayKeys2Hump($this->data);
    }

    public static function success(array $data = [], string $msg = '操作成功', int $code = 0): Response
    {
        return new Response($code, $msg, $data);
    }

    public static function fail(string $msg = '操作失败', array $data = [], int $code = -1): Response
    {
        return new Response($code, $msg, $data);
    }

    public function __toString()
    {
        return json_encode(['code' => $this->code, 'msg' => $this->msg, 'data' => $this->data]);
    }
}