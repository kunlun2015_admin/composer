<?php
/**
 * 数组工具
 * @authors Kunlun (szhcool1129@sina.com)
 * @date    2024-01-30 20:22
 */

namespace Kunlun\base;

class ArrayUtil
{
    /**
     * 把数组的键转驼峰命名
     * @param array $data
     * @return array
     */
    public static function arrayKeys2Hump(array $data): array
    {
        foreach ($data as $k => $v) {
            if (is_array($v)) {
                $data[$k] = self::arrayKeys2Hump($v);
            }
            $dataKey = StringUtil::stringHump($k);
            if ($dataKey != $k) {
                $data[$dataKey] = $v;
                unset($data[$k]);
            }
        }
        return $data;
    }
}