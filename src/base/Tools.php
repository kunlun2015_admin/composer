<?php
/**
 * 常用工具
 * @author Kunlun (szhcool1129@sina.com)
 * @date    2024-06-17 19:44
 */

namespace Kunlun\base;

use Exception;

class Tools
{
    /**
     * 根据目录获取文件列表
     * @param string $dirPath 目录地址
     * @param bool $recursion 是否递归获取
     * @param bool $dirStructure 是否需要目录结构
     * @param string $tempPath 临时变量，无需传参
     * @param array $result 临时变量，无需传参
     * @return array
     */
    public static function getFileListByDir(string $dirPath, bool $recursion = false, bool $dirStructure = false, string $tempPath = '', array $result = []): array
    {
        //获取目录下的文件列表
        $fileList = scandir($dirPath . $tempPath);
        foreach ($fileList as $file) {
            if (in_array($file, ['.', '..'])) {
                continue;
            }
            $currentFile = $tempPath . DIRECTORY_SEPARATOR . $file;
            if (is_dir($dirPath . $currentFile)) {
                if ($recursion) {
                    $result = self::getFileListByDir($dirPath, $recursion, $dirStructure, $currentFile, $result);
                }
            } else {
                if ($tempPath && $dirStructure) {
                    $result[mb_substr($tempPath, 1)][] = $file;
                } else {
                    $result[] = $file;
                }
            }
        }
        return array_filter($result);
    }

    /**
     * 删除目录
     * @param string $dirPath
     * @return void
     * @throws Exception
     */
    public static function deleteDir(string $dirPath): void
    {
        if (!is_dir($dirPath)) {
            throw new Exception('目录不存在');
        }
        //判断目录内是否有文件，如果有先删除文件
        $fileList = scandir($dirPath);
        foreach ($fileList as $file) {
            if (in_array($file, ['.', '..'])) {
                continue;
            }
            if (is_dir($dirPath . DIRECTORY_SEPARATOR . $file)) {
                self::deleteDir($dirPath . DIRECTORY_SEPARATOR . $file);
            } else {
                unlink($dirPath . DIRECTORY_SEPARATOR . $file);
            }
        }
        rmdir($dirPath);
    }
}