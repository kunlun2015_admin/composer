<?php
/**
 * 微信网页授权登录
 * @authors Kunlun (szhcool1129@sina.com)
 * @date    2024-01-23 14:18
 */

namespace Kunlun\wechat;

use Exception;
use Kunlun\base\Curl;

class WebAuthLogin
{
    private static ?WebAuthLogin $instance = null;

    /**
     * @var String 微信应用id
     */
    private string $appId;
    /**
     * @var String 微信应用密钥
     */
    private string $secret;

    private string $pageUrl;

    /**
     * @throws Exception
     */
    private function __construct(string $appId, string $secret)
    {
        $this->appId = $appId;
        $this->secret = $secret;

        if (!$this->appId || !$this->secret) {
            throw new Exception('appId或secret不能为空');
        }
        $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $this->pageUrl = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    /**
     * 获取网页授权实例
     * @throws Exception
     */
    public static function getInstance(string $appId, string $secret): WebAuthLogin
    {
        if (self::$instance !== null) {
            return self::$instance;
        }
        return new self($appId, $secret);
    }

    /**
     * 获取授权code
     * @param string $scope
     * @return string
     */
    public function getWebAuthCode(string $scope = 'snsapi_userinfo'): string
    {
        if (isset($_GET['code']) && $_GET['code']) {
            return $_GET['code'];
        }
        $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' . $this->appId
            . '&redirect_uri=' . urlencode($this->pageUrl) . '&response_type=code&scope='
            . $scope . '&state=STATE#wechat_redirect';
        header('Location:' . $url);
        return '';
    }

    /**
     * 获取网页授权accessToken
     * @param string $scope
     * @return array
     * @throws Exception
     */
    public function getWebAuthAccessToken(string $scope = 'snsapi_userinfo'): array
    {
        $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='
            . $this->appId . '&secret=' . $this->secret . '&code=' . $this->getWebAuthCode($scope) . '&grant_type=authorization_code';
        $response = Curl::get($url);
        if ($response->code != 200 || $response->error) {
            throw new Exception('获取accessToken失败：' . $response->error);
        }
        $result = json_decode($response->response, true);
        if (isset($result['errcode'])) {
            throw new Exception($result['errmsg'], $result['errcode']);
        }
        return $result;
    }

    /**
     * 获取授权用户信息
     * @return array
     * @throws Exception
     */
    public function getUserInfo(): array
    {
        $accessToken = $this->getWebAuthAccessToken();
        $url = 'https://api.weixin.qq.com/sns/userinfo?access_token=' . $accessToken['access_token'] . '&openid=OPENID&lang=zh_CN';
        $response = Curl::get($url);
        if ($response->code != 200 || $response->error) {
            throw new Exception('获取用户信息失败：' . $response->error);
        }
        $result = json_decode($response->response, true);
        if (isset($result['errcode'])) {
            throw new Exception($result['errmsg'], $result['errcode']);
        }
        return $result;
    }

    private function __clone()
    {
    }
}
